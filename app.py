
from distutils.log import debug
from pickle import TRUE
import flask
from flask import Flask, render_template, request
from flask_pymongo import PyMongo
app = Flask(__name__)

mongodb_client = PyMongo(app, uri="mongodb://localhost:27017/login_logout")
db = mongodb_client.db

app.config["MONGO_URI"] = "mongodb://localhost:27017/login_logout"
mongodb_client = PyMongo(app)
db = mongodb_client.db


@app.route('/')
def hello_world():
    return "sandeep gahlawat"


@app.route("/add_one",methods = ['POST'])
def add_one():
    email = request.form.get('email')
    password = request.form.get('password')
    user = db.users.find_one({'email':email})
    if user is not None:
        return flask.jsonify(message="user with this email id is already exist")
    else:
        db.users.insert_one({'email': email, 'password': password})
        return flask.jsonify(message="success")


@app.route('/register')
def register():
    return render_template('register.html')


@app.route('/login')
def login():
    return render_template('login.html')


@app.route('/validation', methods=['POST'])
def validation():
    # if request.method == "POST":
    email = request.form.get('email')
    password = request.form.get('password')
    user = db.users.find_one({'email':email})
    if user is not None:
        return flask.jsonify(message="login in successfully")
    else:
        return "user with this email id is not exist"


if __name__ == "__main__":
    app.run(debug=TRUE, port=8000)
