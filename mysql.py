
from distutils.log import debug
from pickle import TRUE
import flask
from flask import Flask, render_template, request
from flask_mysqldb import MySQL
import MySQLdb.cursors
app = Flask(__name__)

app.config['MYSQL_HOST'] = 'localhost'
app.config['MYSQL_USER'] = 'root'
app.config['MYSQL_PASSWORD'] = ''
app.config['MYSQL_DB'] = 'sql_practice'

mysql = MySQL(app)






@app.route('/')
def hello_world():
    return "sandeep gahlawat"


@app.route("/add_one", methods=['POST'])
def add_one():
    email = request.form.get('email')
    password = request.form.get('password')

    cursor = mysql.connection.cursor(MySQLdb.cursors.DictCursor)
    cursor.execute('SELECT * FROM Users WHERE email LIKE %s',[email])
    data = cursor.fetchone()
    if data :
        return "user with this email is already exist"
    cursor.execute(''' INSERT INTO Users VALUES(%s,%s)''', (email, password))
    mysql.connection.commit()
    
    cursor.close()
    # user = db.users.find_one({'email': email})
    # if user is not None:
    #     return flask.jsonify(message="user with this email id is already exist")
    # else:
    #     db.users.insert_one({'email': email, 'password': password})
    return flask.jsonify(message="success")
   


@app.route('/register')
def register():
    return render_template('register.html')


@app.route('/login')
def login():
    return render_template('login.html')


@app.route('/validation', methods=['POST'])
def validation():
    # if request.method == "POST":
    email = request.form.get('email')
    password = request.form.get('password')
    cursor = mysql.connection.cursor(MySQLdb.cursors.DictCursor)
    cursor.execute('SELECT * FROM Users WHERE email LIKE %s AND password LIKE %s',[email,password])
    data = cursor.fetchone()
    if data :
        return "login in successfully"
    else:
        return "please login with valid credentials"
   


if __name__ == "__main__":
    app.run(debug=TRUE, port=8000)
